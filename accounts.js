/*
 * @file:
 * The file contains API's
 * necessary for interacting with ledgers accounts
 * on Stellar block-chain
 *
 *
 * @Author: omar@clickpesa.com
 * @Date: 2018-12-12 10:50:56
 */

"use strict";
var StellarSdk = require("stellar-sdk");
const config = require("config");
const tools = require("./tools");
const server = tools.getServer();

module.exports = {
  /**
   * @description create random stellar account
   * @type {Object}
   * @param {String} account_id
   */
  createNewAccount: function () {
    var keypair = StellarSdk.Keypair.random();
    return { secret: keypair.secret(), publicKey: keypair.publicKey() };
  },

  /**
   * @description get stellar account details by account_id
   * @type {Object}
   * @param {String} account_id
   */
  getAccountDetails: function (account_id) {
    return new Promise((resolve) => {
      var account = server.accounts().accountId(account_id).call().then();
      resolve(account);
    });
  },
  /**
   * @description get stellar account balance by account_id
   * @type {Object}
   * @param {String} account_id
   */
  getAccountBalances: function (account_id) {
    return new Promise((resolve, reject) => {
      this.getAccountDetails(account_id)
        .then((response) => {
          resolve(response.balances);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  /**
   * @description activate stellar account
   * by deposit starting balance in XLM
   *
   *  @type {Object}
   *  @param {String} source_keyPair
   *  @param {String} new_account_publicKey
   *  @param {String} starting_amount
   */
  activateNewAccount: async function (
    operation_keyPair,
    new_account_publicKey,
    starting_amount
  ) {
    const account = await server.loadAccount(process.env.OPERATIONAL_PUBLIC);
    /*
            Right now, we have one function that fetches the base fee.
            In the future, we'll have functions that are smarter about suggesting fees,
            e.g.: `fetchCheapFee`, `fetchAverageFee`, `fetchPriorityFee`, etc.
        */
    const fee = await tools.getFee();
    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.createAccount({
          destination: new_account_publicKey,
          startingBalance: starting_amount, // in XLM
        })
      )
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(operation_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },

  /**
   * @description set clickpesa multisignature,
   *  inflation destination, and account threshold
   *
   *  @type {Object}
   *  @param {String} new_account_keyPair
   *
   */
  setOptions: async function (new_account_keyPair) {
    const account_publicKey = new_account_keyPair.publicKey();
    const account = await server.loadAccount(account_publicKey);
    const fee = await tools.getFee();

    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.setOptions({
          lowThreshold: 2,
          medThreshold: 3,
          highThreshold: 4,
          masterWeight: 2,
          signer: {
            ed25519PublicKey: process.env.OPERATIONAL_PUBLIC,
            weight: 4,
          },
          inflationDest: process.env.OPERATIONAL_PUBLIC,
        })
      )
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(new_account_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },

  /**
   * @description activate default clickpesa asset
   * by creating trustline between new account and
   * clickpesa asset issuing account
   *
   *  @type {Object}
   *  @param {String} issuing_account_keypair
   *  @param {String} operational_keyPair
   *  @param {String} receiving_account_keyPair
   *
   */
  activateClickPesaDefaultAssets: async function (
    issuing_account_keypair,
    operational_keyPair,
    receiving_account_keyPair
  ) {
    var issuing_account_publicKey = issuing_account_keypair.publicKey();
    var receiving_account_publicKey = receiving_account_keyPair.publicKey();

    // Create an object to represent custom assets
    const assetCodes = process.env.CLICKPESA_DEFAULT_ASSET_CODES.split(",");
    const assets = assetCodes.map(
      (code) => new StellarSdk.Asset(code, issuing_account_publicKey)
    );

    const account = await server.loadAccount(receiving_account_publicKey);
    const fee = await tools.getFee();
    var transaction = new StellarSdk.TransactionBuilder(account, { fee });

    assets.forEach((asset) => {
      transaction = transaction.addOperation(
      StellarSdk.Operation.changeTrust({
        asset: asset
      })
      );
    });

    transaction = transaction.setTimeout(60).build();
    // sign the transaction
    transaction.sign(receiving_account_keyPair);
    transaction.sign(operational_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },

  /**
   * @description validate if account
   * has enough balance to perform a transaction
   *
   * @type {Object}
   * @param {String} account_publicKey
   *  @param {String} asset
   * @param {String} required_amount
   *
   */

  checkAccountBalance: async function (
    account_publicKey,
    asset,
    required_amount
  ) {
    let isNative = asset == "XLM" ? true : false;
    let account_balance = await this.getAccountBalances(account_publicKey);
    if (!account_balance) {
      return new Promise((resolve) => {
        resolve(false);
      });
    }
    if (isNative) {
      var asset_balance = account_balance.filter(
        (accountAsset) => accountAsset.asset_type == "native"
      );
    } else {
      asset_balance = account_balance.filter(
        (accountAsset) => accountAsset.asset_code == asset
      );
    }
    let balance_amount = asset_balance[0].balance;
    let hasSufficientBalance =
      Number(required_amount) <= balance_amount ? true : false;
    return new Promise((resolve) => {
      resolve(hasSufficientBalance);
    });
  },
};
