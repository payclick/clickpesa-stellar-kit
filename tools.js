/*
 * @file:
 * The file contains various setup configuration
 * necessary for setting up ledgers with
 * Stellar block-chain
 *
 *
 * @Author: omar@clickpesa.com
 * @Date: 2018-12-12 10:50:56
 */

"use strict";
const config = require("config");
const StellarSdk = require("stellar-sdk");

function createStellarHorizonServer(network) {
  let server;
  switch (network) {
    case "TESTNET":
      StellarSdk.Network.useTestNetwork();
      server = new StellarSdk.Server("https://horizon-testnet.stellar.org");
      break;
    case "PUBLIC":
      StellarSdk.Network.usePublicNetwork();
      server = new StellarSdk.Server("https://horizon.stellar.org");
      break;

    default:
      console.log("Error :", "Switching networks");
      break;
  }
  return server;
}

const server = createStellarHorizonServer(config.get("HORIZON_NETWORK"));

module.exports = {
  /**
   * @description create stellar horizon server
   * @type {Object}
   */
  getServer: function () {
    return server;
  },
  /**
   * @description create account keyPair
   * @type {Object}
   * @param {String} secretKey
   */
  createKeypair: (secretKey) => {
    return StellarSdk.Keypair.fromSecret(secretKey);
  },

  /**
   * @description create publicKey from keyPair
   * @type {Object}
   * @param {String} keyPair
   */
  createPublicWithKeyPair: function (keyPair) {
    return keyPair.publicKey();
  },

  /**
   * @description create publicKey from secret
   * @type {Object}
   * @param {String} secret
   */
  createPublicWithSecret: function (secret) {
    var keyPair = StellarSdk.Keypair.fromSecret(secret);
    var publicKey = keyPair.publicKey();
    return publicKey;
  },
  /**
   * @description create custom asset on stellar network
   * @type {Object}
   * @param { String } asset_name
   * @param { String } issuing_publicKey
   */
  createCustomAsset: function (asset_name, issuing_publicKey) {
    return new StellarSdk.Asset(asset_name, issuing_publicKey);
  },

  /**
   * @description transfers tax and fees amount
   * for every transfer to the operations account.
   * (can be any account responsible
   *  for  tax and fees collections)
   *
   * @type {Object}
   * @param {String} operational_account_keyPair
   * @param {String} charges_account_keyPair
   * @param {String} tax_and_fee_amount
   * @param {String} asset
   */
  collectTaxandFees: async (
    operational_account_keyPair,
    charges_account_keyPair,
    amount,
    asset
  ) => {
    const charges_account__publicKey = charges_account_keyPair.publicKey();
    const account = await server.loadAccount(process.env.OPERATIONAL_PUBLIC);
    const fee = await this.getFee();
    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.payment({
          destination: process.env.OPERATIONAL_PUBLIC,
          amount: amount.toString(),
          asset: asset,
          source: charges_account__publicKey,
        })
      )
      .addMemo(StellarSdk.Memo.text("Transfer fees and tax"))
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(charges_account_keyPair);
    transaction.sign(operational_account_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },
  /**
   * @description retrieve transfer fees
   */
  async getFee() {
    const baseFeeMultipler = process.env.STELLAR_BASE_FEE_MULTIPLIER;
    const baseFee = await server.fetchBaseFee();
    const fee = +baseFee * +baseFeeMultipler;
    return fee.toString();
  },
};
