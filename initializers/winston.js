/*
 * @file:  Winston application logger initializer
 * @Author: omar@clickpesa.com
 * @Date: 2019-05-21 04:06:51 
 */

'use strict';

//dependencies
const { format, createLogger, transports } = require('winston');
const SlackHook = require("winston-slack-webhook-transport");
let d = new Date()

var fileLogger = createLogger({
    transports: [,
        new (transports.File)({
            level: 'error',
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.errors({ stack: true }),
                format.splat(),
                format.json()),
            filename: './logs/logs.json'
        })
    ]
});

var slackLogger = createLogger({
    transports: [
        new SlackHook({
            level: "info",
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                format.errors({ stack: true }),
                format.splat(),
                format.json()),
            webhookUrl: "https://hooks.slack.com/services/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",
            formatter: info => {
                return {
                    "attachments": [
                        {
                            "fallback": `Stellar Kit`,
                            "color": "#fddc01",
                            "author_name": `${info.operation}`,
                            "author_link": "https://bitbucket.org/OmmyJay/clickpesa-stellar-kit/src/master/",
                            "author_icon": "https://www.stellar.org/wp-content/themes/stellar/images/stellar-rocket-300.png",
                            "title": `${info.message}`,
                            "title_link": info.link,
                            "text": info.extra,
                            "fields": [
                                {
                                    "title": "Created at",
                                    "value": d.toLocaleString(),
                                    "short": false
                                },
                                {
                                    "title": "Issue type",
                                    "value": info.issue_type,
                                    "short": false
                                }
                            ],
                            "footer": "ClickPesa Stellar API",
                            "footer_icon": "http://clickpesa.com/wp-content/uploads/2019/01/logo_compact.png"
                        }
                    ]
                }
            }
        })
    ]
});

module.exports = {
    fileLogger, 
    slackLogger
};
