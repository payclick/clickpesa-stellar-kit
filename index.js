const accounts = require("./accounts");
const payments = require("./payments");
const tools = require("./tools");
const config = require("config");
var issuing_secret = process.env.ISSUING_SECRET;
var operational_secret = process.env.OPERATION_SECRET;
const { slackLogger, fileLogger } = require("./initializers/winston");
const Sentry = require("@sentry/node");
var numeral = require("numeral");

//initilize sentry
Sentry.init({
  dsn: "https://f87ce9710d9b4a5c9d6452ee0874b806@sentry.io/1463993",
});


// Withdrawal Queues
const withdrawalQueue = [];
let isProcessing = false;

async function addToWithdrawQueue(
  withdrawal_account_secret,
  withdrawal_amount,
  tax_and_fees,
  charge_account_secret,
  asset,
  tax_fee_asset
) {
  return new Promise((resolve, reject) => {
    withdrawalQueue.push({
      withdrawal_account_secret,
      withdrawal_amount,
      tax_and_fees,
      charge_account_secret,
      asset,
      tax_fee_asset,
      resolve, // Store resolve and reject functions in the queue item
      reject,
    });

    if (!isProcessing) {
      processWithdrawQueue();
    }
  });
}

async function processWithdrawQueue() {
  if (withdrawalQueue.length === 0) {
    isProcessing = false;
    return;
  }

  isProcessing = true;
  const currentRequest = withdrawalQueue.shift(); // Get the first item from the queue
  const {
    withdrawal_account_secret,
    withdrawal_amount,
    tax_and_fees,
    charge_account_secret,
    asset,
    tax_fee_asset,
    resolve, // Extract resolve and reject from the current request
    reject
  } = currentRequest;

  // Retry logic for the current transaction
  const retries = +process.env.STELLAR_WALLET_KIT_QUEUE_RETRIES;
  const delay = +process.env.STELLAR_WALLET_KIT_QUEUE_DELAY

  for (let i = 0; i < retries; i++) {
    try {
      let operation_keyPair = await tools.createKeypair(operational_secret);
      let charges_account_keyPair = await tools.createKeypair(charge_account_secret);
      let withdrawal_keyPair = await tools.createKeypair(withdrawal_account_secret);

      // Perform the withdrawal
      let result = await payments.withdrawalFund(
        withdrawal_keyPair,
        operation_keyPair,
        withdrawal_amount,
        tax_and_fees,
        charges_account_keyPair,
        asset,
        tax_fee_asset
      );

      if (result.hash) {
        sendWithdrawalSlackLogger(result.hash);
        console.log("Withdrawal successful:", result.hash);
      }

      resolve(result); // Resolve the promise for the current request
      break; // Exit the retry loop on success

    } catch (error) {
      if (error.response && error.response.status === 504) {
        console.error(`Received 504 error, retrying... (${i + 1}/${retries})`);

        if (i === retries - 1) {
          reject(new Error(`Max retries reached. Failed to process withdrawal.`));
        } else {
          // Wait for the delay before retrying
          await new Promise((resolve) => setTimeout(resolve, delay));
        }

      } else {
        // If it's another type of error, reject immediately
        console.error("Withdrawal failed:", error.message);
        reject(error);
        break; // Stop retrying on non-504 errors
      }
    }
  }

  // Process the next withdrawal in the queue after current retries are exhausted
  processWithdrawQueue();
}


const transferQueue = [];
let isProcessingTransfer = false;

async function addToTransferQueue(
  sending_account_secret,
  charges_account_secret,
  destination,
  amount_after_tax,
  tax_and_fees,
  amount_asset,
  tax_fee_asset
) {
  return new Promise((resolve, reject) => {
    transferQueue.push({
      sending_account_secret,
      charges_account_secret,
      destination,
      amount_after_tax,
      tax_and_fees,
      amount_asset,
      tax_fee_asset,
      resolve, // Store resolve and reject functions in the queue item
      reject,
    });

    if (!isProcessingTransfer) {
      processTransferQueue();
    }
  });
}

async function processTransferQueue() {
  if (transferQueue.length === 0) {
    isProcessingTransfer = false;
    return;
  }

  isProcessingTransfer = true;
  const currentRequest = transferQueue.shift(); // Get the first item from the queue
  const {
    sending_account_secret,
    charges_account_secret,
    destination,
    amount_after_tax,
    tax_and_fees,
    amount_asset,
    tax_fee_asset,
    resolve, // Extract resolve and reject from the current request
    reject,
  } = currentRequest;

  // Retry logic for the current transfer
  const retries = +process.env.STELLAR_WALLET_KIT_QUEUE_RETRIES;
  const delay = +process.env.STELLAR_WALLET_KIT_QUEUE_DELAY

  for (let i = 0; i < retries; i++) {
    try {
      let sending_keyPair = await tools.createKeypair(sending_account_secret);
      let charge_keyPair = await tools.createKeypair(charges_account_secret);
      let operation_keyPair = await tools.createKeypair(operational_secret);

      // Perform the transfer
      let result = await payments.transfer(
        sending_keyPair,
        operation_keyPair,
        destination,
        amount_after_tax,
        amount_asset,
        tax_and_fees,
        tax_fee_asset,
        charge_keyPair
      );

      if (result.hash) {
        sendTransferSlackLogger(result.hash);
        console.log("Transfer successful:", result.hash);
      }

      resolve(result); // Resolve the promise for the current request
      break; // Exit the retry loop on success

    } catch (error) {
      console.log('👨‍⚖️ Transfer error:', error);

      if (error.response && error.response.status === 504) {
        console.error(`Received 504 error, retrying... (${i + 1}/${retries})`);

        if (i === retries - 1) {
          reject(new Error(`Max retries reached. Failed to process transfer.`));
        } else {
          // Wait for the delay before retrying
          await new Promise((resolve) => setTimeout(resolve, delay));
        }

      } else {
        // If it's another type of error, reject immediately
        console.error("Transfer failed:", error.message);
        reject(error);
        break; // Stop retrying on non-504 errors
      }
    }
  }

  // Process the next transfer in the queue after the current retries are exhausted
  processTransferQueue();
}

const depositQueue = [];
let isProcessingDeposit = false;

async function addToDepositQueue(destination_publicKey, amount, asset) {
  return new Promise((resolve, reject) => {
    depositQueue.push({
      destination_publicKey,
      amount,
      asset,
      resolve, // Store resolve and reject functions in the queue item
      reject,
    });

    if (!isProcessingDeposit) {
      processDepositQueue();
    }
  });
}

async function processDepositQueue() {
  if (depositQueue.length === 0) {
    isProcessingDeposit = false;
    return;
  }

  isProcessingDeposit = true;
  const currentRequest = depositQueue.shift(); // Get the first item from the queue
  const {
    destination_publicKey,
    amount,
    asset,
    resolve, // Extract resolve and reject from the current request
    reject,
  } = currentRequest;

  // Retry logic for the current deposit
  const retries = +process.env.STELLAR_WALLET_KIT_QUEUE_RETRIES;
  const delay = +process.env.STELLAR_WALLET_KIT_QUEUE_DELAY

  for (let i = 0; i < retries; i++) {
    try {
      let operational_account_keyPair = await tools.createKeypair(operational_secret);

      // Perform the deposit
      let result = await payments.depositFund(
        operational_account_keyPair,
        destination_publicKey,
        amount,
        asset
      );

      if (result.hash) {
        sendDepositSlackLogger(result.hash);
        console.log("Deposit successful:", result.hash);
      }

      resolve(result); // Resolve the promise for the current request
      break; // Exit the retry loop on success

    } catch (error) {
      console.log('👨‍⚖️ Deposit error:', error);

      if (error.response && error.response.status === 504) {
        console.error(`Received 504 error, retrying... (${i + 1}/${retries})`);

        if (i === retries - 1) {
          reject(new Error(`Max retries reached. Failed to process deposit.`));
        } else {
          // Wait for the delay before retrying
          await new Promise((resolve) => setTimeout(resolve, delay));
        }

      } else {
        // If it's another type of error, reject immediately
        console.error("Deposit failed:", error.message);
        reject(error);
        break; // Stop retrying on non-504 errors
      }
    }
  }

  // Process the next deposit in the queue after the current retries are exhausted
  processDepositQueue();
}


module.exports = {
  /**
   * @description expose clickpesa
   * public key(s)
   */
  KEYS: {
    OPERATIONAL_PUBLIC_KEY: process.env.OPERATIONAL_PUBLIC,
  },
  /**
   * @description creates new stellar account
   *
   */
  createNewAccoount: async function () {
    //Create new account
    let operation_keyPair = await tools.createKeypair(operational_secret);
    let new_account = await accounts.createNewAccount();
    let result = await accounts.activateNewAccount(
      operation_keyPair,
      new_account.publicKey,
      "5"
    );
    let hash = result.hash; // transaction proof
    //set ckickpesa multisign,inflation options
    let new_account_keyPair = await tools.createKeypair(new_account.secret);
    await accounts.setOptions(new_account_keyPair);
    // activate clickpesa assets
    let issuing_account_keyPair = await tools.createKeypair(issuing_secret);
    await accounts.activateClickPesaDefaultAssets(
      issuing_account_keyPair,
      operation_keyPair,
      new_account_keyPair
    );
    //retrieve new account balances
    let new_account_balance = await accounts.getAccountBalances(
      new_account.publicKey
    );
    //send slack logger
    if (new_account_balance) {
      sendNewAccountSlackLogger(new_account.publicKey);
    }
    return new Promise((resolve) => {
      resolve({
        publicKey: new_account.publicKey,
        secret: new_account.secret,
        balance: new_account_balance,
        hash: hash,
      });
    });
  },

  /**
   * @description transfer asset
   * @type {object}
   * @param {String} sending_account_secret
   * @param {String} charges_account_secret
   * @param {String} destination_publicKey
   * @param {String} amount_after_tax
   * @param {String} tax_and_fees_amount
   * @param {String} amount_asset
   * @param {String} tax_fee_asset
   */
  transfer: async (
    sending_account_secret,
    charges_account_secret,
    destination,
    amount_after_tax,
    tax_and_fees,
    amount_asset,
    tax_fee_asset,
  ) => {
    if (amount_after_tax == 0) {
      return new Promise((reject) => {
        reject(new Error("Invalid sending or tax amount value, stop playin."));
      });
    }

    return await addToTransferQueue(
      sending_account_secret,
      charges_account_secret,
      destination,
      amount_after_tax,
      tax_and_fees,
      amount_asset,
      tax_fee_asset
    );
  },
  /**
   * @description checks if account has
   * balance above certain amount
   * @type {boolean}
   * @param {String} account_publicKey
   * @param {String} asset
   * @param {String} required_amount
   *
   */
  balancesCheck: async (account_publicKey, asset, required_amount) => {
    if (required_amount == 0) {
      return new Promise((reject) => {
        reject(new Error("Invalid amount value, stop playin."));
      });
    }
    let result = await accounts.checkAccountBalance(
      account_publicKey,
      asset,
      required_amount
    );
    return new Promise((resolve) => {
      resolve(result);
    });
  },
  /**
   * @description makes deposits
   * @type {object}
   * @param {String} destination_publicKey
   * @param {String} amount
   * @param {String} asset
   *
   */
  deposit: async (destination_publicKey, amount, asset) => {
    if (amount == 0) {
      return new Promise((reject) => {
        reject(new Error("Invalid amount value, stop playin."));
      });
    }
    return await addToDepositQueue(destination_publicKey, amount, asset);
  },
  /**
   * @description makes withdrawals
   * @type {object}
   * @param {String} withdrawal_account_secret
   * @param {String} withdrawal_amount
   * @param {String} tax_and_fees
   * @param {String} charge_account_secret
   * @param {String} asset
   * @param {String} tax_fee_asset
   */
  withdrawal: async ({
    withdrawal_account_secret,
    withdrawal_amount,
    tax_and_fees,
    charge_account_secret,
    asset,
    tax_fee_asset,
  }) => {
    if (withdrawal_amount == 0) {
      return new Promise((reject) => {
        reject(new Error("Invalid sending or tax amount value, stop playin."));
      });
    }
    return await addToWithdrawQueue(
      withdrawal_account_secret,
      withdrawal_amount,
      tax_and_fees,
      charge_account_secret,
      asset,
      tax_fee_asset
    );
  },
  /**
   * @description gets account new balances
   *
   * @param {String} account_publicKey
   *
   */
  getNewBalance: async (account_publicKey) => {
    let result = await accounts.getAccountBalances(account_publicKey);
    return new Promise((resolve, reject) => {
      resolve(result);
      reject(result);
    });
  },
  singleSignTransfer: async (sending_account_secret, destination, amount, asset) => {
    if (amount == 0) {
      return new Promise((reject) => {
        reject(new Error("Invalid sending or tax amount value, stop playin."));
      });
    }
    let sending_keyPair = await tools.createKeypair(sending_account_secret);
    let result = await payments.singleSignTransfer(
      sending_keyPair,
      destination,
      amount,
      asset
    );
    if (result.hash) {
      sendTransferSlackLogger(result.hash);
    }
    return new Promise((resolve, reject) => {
      resolve(result);
      reject(new Error("Payment Unsucceful"));
    });
  },
};

async function sendNewAccountSlackLogger(account) {
  let operational_balance = await accounts.getAccountBalances(
    process.env.OPERATIONAL_PUBLIC
  );
  let extra_info = `
        New Balance:\n
        ${operational_balance[0].asset_code} : ${numeral(
    operational_balance[0].balance
  ).format("0,0.00")}\n
        ${operational_balance[2].asset_code} : ${numeral(
    operational_balance[2].balance
  ).format("0,0.00")}\n
        ${operational_balance[1].asset_code} : ${numeral(
    operational_balance[1].balance
  ).format("0,0.00")}\n
        ${operational_balance[3].asset_code} : ${numeral(
    operational_balance[3].balance
  ).format("0,0.00")}\n
        LUMENS : ${numeral(operational_balance[4].balance).format("0,0.00")}
        `;
  slackLogger.info("%s", `${account}`, {
    operation: "New account created",
    issue_type: "Info",
    extra: extra_info,
    link: `https://horizon-testnet.stellar.org/accounts/${account}`,
  });
}

async function sendTransferSlackLogger(hash) {
  let operational_balance = await accounts.getAccountBalances(
    process.env.OPERATIONAL_PUBLIC
  );
  let extra_info = `
        New Balance:\n
        ${operational_balance[0].asset_code} : ${numeral(
    operational_balance[0].balance
  ).format("0,0.00")}\n
        ${operational_balance[2].asset_code} : ${numeral(
    operational_balance[2].balance
  ).format("0,0.00")}\n
        ${operational_balance[1].asset_code} : ${numeral(
    operational_balance[1].balance
  ).format("0,0.00")}\n
        ${operational_balance[3].asset_code} : ${numeral(
    operational_balance[3].balance
  ).format("0,0.00")}\n
        LUMENS : ${numeral(operational_balance[4].balance).format("0,0.00")}
        `;
  slackLogger.info("%s", `${hash}`, {
    operation: "Success Transfer",
    issue_type: "Info",
    extra: extra_info,
    link: `https://horizon-testnet.stellar.org/transactions/${hash}/payments`,
  });
}

async function sendDepositSlackLogger(hash) {
  let operational_balance = await accounts.getAccountBalances(
    process.env.OPERATIONAL_PUBLIC
  );
  let extra_info = `
        New Balance:\n
        ${operational_balance[0].asset_code} : ${numeral(
    operational_balance[0].balance
  ).format("0,0.00")}\n
        ${operational_balance[2].asset_code} : ${numeral(
    operational_balance[2].balance
  ).format("0,0.00")}\n
        ${operational_balance[1].asset_code} : ${numeral(
    operational_balance[1].balance
  ).format("0,0.00")}\n
        ${operational_balance[3].asset_code} : ${numeral(
    operational_balance[3].balance
  ).format("0,0.00")}\n
        LUMENS : ${numeral(operational_balance[4].balance).format("0,0.00")}
        `;
  slackLogger.info("%s", `${hash}`, {
    operation: "Success Deposit",
    issue_type: "Info",
    extra: extra_info,
    link: `https://horizon-testnet.stellar.org/transactions/${hash}/payments`,
  });
}

async function sendWithdrawalSlackLogger(hash) {
  let operational_balance = await accounts.getAccountBalances(
    process.env.OPERATIONAL_PUBLIC
  );
  let extra_info = `
        New Balance:\n
        ${operational_balance[0].asset_code} : ${numeral(
    operational_balance[0].balance
  ).format("0,0.00")}\n
        ${operational_balance[2].asset_code} : ${numeral(
    operational_balance[2].balance
  ).format("0,0.00")}\n
        ${operational_balance[1].asset_code} : ${numeral(
    operational_balance[1].balance
  ).format("0,0.00")}\n
        ${operational_balance[3].asset_code} : ${numeral(
    operational_balance[3].balance
  ).format("0,0.00")}\n
        LUMENS : ${numeral(operational_balance[4].balance).format("0,0.00")}
        `;
  slackLogger.info("%s", `${hash}`, {
    operation: "Success Withdrawal",
    issue_type: "Info",
    extra: extra_info,
    link: `https://horizon-testnet.stellar.org/transactions/${hash}/payments`,
  });
}

