/*
 * @file:
 * The file contains API's
 * necessary for making payments
 * on Stellar block-chain
 *
 *
 * @Author: omar@clickpesa.com
 * @Date: 2018-12-12 10:50:56
 */

"use strict";

const config = require("config");
const StellarSdk = require("stellar-sdk");
const tools = require("./tools");
const server = tools.getServer();
const accounts = require("./accounts");

module.exports = {
  /**
   * @description transfer asset from one
   * stellar account to another (peer-to-peer)
   * @type {Object}
   * @param {String} sending_account_keyPair
   * @param {String} operational_account_keyPair
   * @param {String} receiving_account_publicKey
   * @param {String} amount
   * @param {String} tax_and_fees
   * @param {String} charges_account_keyPair
   * @param {String} amount_asset
   * @param {String} tax_fee_asset
   */
  transfer: async (
    sending_account_keyPair,
    operational_account_keyPair,
    receiving_account,
    amount,
    amount_asset,
    tax_and_fees,
    tax_fee_asset,
    charges_account_keyPair
  ) => {
    const sending_acount_publicKey = await tools.createPublicWithKeyPair(
      sending_account_keyPair
    );

    //Validate sender account balance
    let hasBalance = await accounts.checkAccountBalance(
      sending_acount_publicKey,
      amount_asset,
      amount
    );
    if (!hasBalance) {
      return new Promise((reject) => {
        reject("Sender account has inssufiecient balance.");
      });
    }

    //Validate charge account balance
    let charge_account_publicKey = await tools.createPublicWithKeyPair(
      charges_account_keyPair
    );
    let hasChargeBalance = await accounts.checkAccountBalance(
      charge_account_publicKey,
      tax_fee_asset,
      tax_and_fees
    );
    if (!hasChargeBalance) {
      return new Promise((reject) => {
        reject(
          "Charge account has inssufiecient balance to cover tax and fees."
        );
      });
    }

    let sending_asset_instance =
      amount_asset === "XLM"
        ? new StellarSdk.Asset.native()
        : new StellarSdk.Asset(amount_asset, process.env.ISSUING_PUBLIC);
    let tax_fee_asset_instance =
      tax_fee_asset === "XLM"
        ? new StellarSdk.Asset.native()
        : new StellarSdk.Asset(tax_fee_asset, process.env.ISSUING_PUBLIC);

    const account = await server.loadAccount(sending_acount_publicKey);
    const fee = await tools.getFee();
    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.payment({
          destination: receiving_account,
          amount: amount,
          asset: sending_asset_instance,
          source: sending_acount_publicKey,
        })
      )
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(sending_account_keyPair);
    transaction.sign(operational_account_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    //collect tax and fees
    if (tax_and_fees > 0) {
      await tools.collectTaxandFees(
        operational_account_keyPair,
        charges_account_keyPair,
        tax_and_fees,
        tax_fee_asset_instance
      );
    }
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },

  singleSignTransfer: async (
    sending_account_keyPair,
    receiving_account,
    amount,
    asset
  ) => {
    const sending_acount_publicKey = await tools.createPublicWithKeyPair(
      sending_account_keyPair
    );

    //Validate sender account balance
    let hasBalance = await accounts.checkAccountBalance(
      sending_acount_publicKey,
      asset,
      amount
    );
    if (!hasBalance) {
      return new Promise((reject) => {
        reject("Sender account has inssufiecient balance.");
      });
    }

    let sending_asset_instance =
      asset === "XLM"
        ? new StellarSdk.Asset.native()
        : new StellarSdk.Asset(asset, process.env.ISSUING_PUBLIC);

    const account = await server.loadAccount(sending_acount_publicKey);
    const fee = await tools.getFee();
    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.payment({
          destination: receiving_account,
          amount: amount,
          asset: sending_asset_instance,
          source: sending_acount_publicKey,
        })
      )
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(sending_account_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },

  /**
   * @description deposit funds from
   *  operational account
   * @type {Object}
   * @param {String} operational_account_keyPair
   * @param {String} receiving_account_publicKey
   * @param {String} amount
   * @param {String} asset
   */
  depositFund: async (
    operational_account_keyPair,
    receiving_account_publickey,
    amount,
    asset
  ) => {
    let sending_asset =
      asset === "XLM"
        ? new StellarSdk.Asset.native()
        : new StellarSdk.Asset(asset, process.env.ISSUING_PUBLIC);
    const account = await server.loadAccount(process.env.OPERATIONAL_PUBLIC);
    const fee = await tools.getFee();
    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.payment({
          destination: receiving_account_publickey,
          amount: amount,
          asset: sending_asset,
          source: process.env.OPERATIONAL_PUBLIC,
        })
      )
      .addMemo(StellarSdk.Memo.text("Deposit Transaction"))
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(operational_account_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },

  /**
   * @description withdrawa funds from
   *  an account into clickpesa operational account
   * @type {Object}
   * @param {String} withdrawa_account_keyPair
   * @param {String} operational_account_keyPair
   * @param {String} receiving_account_publicKey
   * @param {String} amount
   * @param {String} tax_and_fees
   * @param {String} charges_account_keyPair
   * @param {String} asset
   * @param {String} tax_fee_asset
   */
  withdrawalFund: async (
    withdrawa_account_keyPair,
    operational_account_keyPair,
    amount,
    tax_and_fees,
    charges_account_keyPair,
    asset,
    tax_fee_asset
  ) => {
    const withdrawal_acount_publicKey = await tools.createPublicWithKeyPair(
      withdrawa_account_keyPair
    );

    //Validate sender account balance
    let hasBalance = await accounts.checkAccountBalance(
      withdrawal_acount_publicKey,
      asset,
      amount
    );
    if (!hasBalance) {
      return new Promise((reject) => {
        reject(`Withdrawal account has insufficient ${asset} balance.`);
      });
    }

    //Validate charge account balance
    let charge_account_publicKey = await tools.createPublicWithKeyPair(
      charges_account_keyPair
    );
    let hasChargeBalance = await accounts.checkAccountBalance(
      charge_account_publicKey,
      tax_fee_asset,
      tax_and_fees
    );
    if (!hasChargeBalance) {
      return new Promise((reject) => {
        reject(
          `Charge account has Insufficient ${tax_fee_asset} balance. to cover tax and fees.`
        );
      });
    }

    let sending_asset_instance =
      asset === "XLM"
        ? new StellarSdk.Asset.native()
        : new StellarSdk.Asset(asset, process.env.ISSUING_PUBLIC);
    let tax_fee_asset_instance =
      tax_fee_asset === "XLM"
        ? new StellarSdk.Asset.native()
        : new StellarSdk.Asset(tax_fee_asset, process.env.ISSUING_PUBLIC);

    const account = await server.loadAccount(process.env.OPERATIONAL_PUBLIC);
    const fee = await tools.getFee();
    const transaction = new StellarSdk.TransactionBuilder(account, { fee })
      .addOperation(
        StellarSdk.Operation.payment({
          destination: process.env.OPERATIONAL_PUBLIC,
          amount: amount.toString(),
          asset: sending_asset_instance,
          source: withdrawal_acount_publicKey,
        })
      )
      .setTimeout(30)
      .build();
    // sign the transaction
    transaction.sign(withdrawa_account_keyPair);
    transaction.sign(operational_account_keyPair);
    const transactionResult = await server.submitTransaction(transaction);
    //collect tax and fees
    if (tax_and_fees > 0)
      await tools.collectTaxandFees(
        operational_account_keyPair,
        charges_account_keyPair,
        tax_and_fees,
        tax_fee_asset_instance
      );
    return new Promise((resolve) => {
      resolve(transactionResult);
    });
  },
};
